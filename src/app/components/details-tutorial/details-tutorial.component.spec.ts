import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsTutorialComponent } from './details-tutorial.component';

describe('DetailsTutorialComponent', () => {
  let component: DetailsTutorialComponent;
  let fixture: ComponentFixture<DetailsTutorialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsTutorialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsTutorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
