import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddTutorialComponent } from './components/add-tutorial/add-tutorial.component';
import { DetailsTutorialComponent } from './components/details-tutorial/details-tutorial.component';
import { ListTutorialComponent } from './components/list-tutorial/list-tutorial.component';

@NgModule({
  declarations: [
    AppComponent,
    AddTutorialComponent,
    DetailsTutorialComponent,
    ListTutorialComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
